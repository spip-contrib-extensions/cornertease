<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;
 
$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cornertease_titre' => 'CornerTease',
	'cfg_article' => 'Article',
	'cfg_duree' => 'Durée',
	'cfg_duree_explication' => 'Durée d\'affichage quand déplié au chargement (avant d\'être replié automatiquement).',
	'cfg_mode_affichage' => 'Mode d\'affichage',
	'cfg_mode_affichage_each' => 'Déplié à chaque nouvelle visite (session)',
	'cfg_mode_affichage_first' => 'Déplié à la première visite (cookie)',
	'cfg_mode_affichage_allways' => 'Toujours déplié',
	'cfg_mode_affichage_never' => 'Jamais déplié',
	'cfg_article_explication' => 'Cliquer sur [Modifier] pour choisir l\'article à afficher dans le coin incitatif.',
	'cfg_titre_parametrages' => 'Paramétrages du coin incitatif',

	// T
	'titre_page_configurer_cornertease' => 'Configuration CornerTease',
);

?>