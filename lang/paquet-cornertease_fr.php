<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cornertease_description' => 'Coin de page dépliable pour inciter à lire un article',
	'cornertease_nom' => 'CornerTease',
	'cornertease_slogan' => 'Excitez la curiosité de vos visiteurs !',
);

?>